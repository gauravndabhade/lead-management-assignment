from django.contrib import admin
from .models import Lead, FollowUp

class LeadAdmin(admin.ModelAdmin):
    fields = ['contact_person_name','address','source', 'phone']


class FollowUpAdmin(admin.ModelAdmin):
    fields = ['response', 'followup_date']


admin.site.register(Lead, LeadAdmin)
admin.site.register(FollowUp, FollowUpAdmin)