from django.db import models
from datetime import date

class Lead(models.Model):
    lead_id = models.AutoField(primary_key=True)
    contact_person_name = models.CharField(max_length=150, blank=True)
    address = models.CharField(max_length=250, blank=True)
    source = models.CharField(max_length=250, blank=True)
    phone = models.CharField(max_length=12, blank=True)    

    def __str__(self):
        return f'{self.contact_person_name} from {self.source}'

class FollowUp(models.Model):
    followup_id = models.AutoField(primary_key=True)
    response = models.CharField(max_length=250, blank=True)
    followup_date = models.DateField(default=date.today, blank=True)
    lead = models.ForeignKey(
        'Lead',
        on_delete=models.CASCADE,
        verbose_name="Lead",
    )

    def __str__(self):
        return f'{self.response} on {self.followup_date}'
