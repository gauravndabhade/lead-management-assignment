import django_filters
from django_filters import DateFilter, DateRangeFilter, CharFilter
from .models import Lead, FollowUp
from django.forms.widgets import TextInput


class FollowUpFilter(django_filters.FilterSet):
    response = CharFilter(field_name='response', 
        widget=TextInput(attrs={'placeholder': 'follow up message'}))

    start_date = DateFilter(field_name='followup_date',lookup_expr='gt',
        widget=TextInput(attrs={'placeholder': 'mm/dd/yyyy'})) 
    
    end_date = DateFilter(field_name='followup_date',lookup_expr='lt',
        widget=TextInput(attrs={'placeholder': 'mm/dd/yyyy'}))
    
    class Meta:
        model = FollowUp
        fields = ['response', 'start_date', 'end_date', 'lead']
        