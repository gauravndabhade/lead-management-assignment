from django.conf.urls import url
from lead.views import followup_list

urlpatterns = [
    url(r'', followup_list, name='leads'),
]