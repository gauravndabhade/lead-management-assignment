from django.shortcuts import render
from .filters import FollowUpFilter
from .models import FollowUp

def followup_list(request):
    filter = FollowUpFilter(request.GET, queryset=FollowUp.objects.all())
    return render(request, 'main.html', {'filter': filter})
